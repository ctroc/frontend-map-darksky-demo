//React Component
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
//App Component
import MapContainer from './maps/Index';
/**
 * Create Routes
 */
function Routes(props) {
  return (
    <BrowserRouter>
      <>
        <Switch>
          <Route path="/" component={MapContainer} exact />
        </Switch>
      </>
    </BrowserRouter>
  );
}
export default Routes;
