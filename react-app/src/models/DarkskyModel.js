import Model from './Model';
/*
 * Product Model Class
 */
class DarkskyModel extends Model {
  constructor() {
    super();
    this.baseUrl = `https://api-weather-express-redis.herokuapp.com`;
    this.headers =  `{ headers: { 'Content-Type': 'application/json' } }`;
  }
  /**
  Details: Get Weather by latitude
  Params: weatherQuery, handleOpen, handleSetWeather
  Return: {}
  */
  getWeather = async (weatherQuery, handleOpen, handleSetWeather) => {
    const randomFailure =  Math.floor(Math.random() * Math.floor(9))
    handleOpen();
    if(randomFailure){
      let url = `${this.baseUrl}/api/${weatherQuery.lat},${weatherQuery.lon}`;
      let response = {data:{ data: {}, error: true}};
      try {
        response = await this.get(url, this.headers);
      } catch (e) {
        console.log(e);
      }
      handleSetWeather({ status: true, event: weatherQuery, ...response.data });
    } else {
      this.getWeather(weatherQuery, handleOpen, handleSetWeather);
    }
  }
}//End class

export default DarkskyModel;
