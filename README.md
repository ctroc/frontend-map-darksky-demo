#Desafio Acid Lab

## Deploy locahost:

#### Clone this repo and go inside to root folder, run this comands:

- docker-compose build
- docker-compose up

## Deploy in Heroku container:

#### Clone this repo, install heroku cli and go inside to root folder, run this comands:

- heroku login
- heroku container:login
- heroku create name_your_app
- heroku container:push web --app name_your_app
- heroku container:release web --app name_your_app
