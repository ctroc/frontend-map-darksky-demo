FROM node:latest
WORKDIR /src
COPY /react-app ./
RUN npm install  && npm run build && npm install -g serve
EXPOSE 5000
CMD serve -s build 
